﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class Soba
    {
        public int Id { get; set; }

        [Required]
        public int BrojSobe { get; set; }
        [Required]
        public int BrojKreveta { get; set; }
        [Required]
        public double CenaPoNocenju { get; set; }

        public int SmestajId { get; set; }
        public Smestaj Smestaj { get; set; }

        public List<Rezervacija> ListRezervacija { get; set; }
    }
}