﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class Smestaj
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name="Name")]
        public string Naziv { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Description")]
        public string Opis { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Adress")]
        public string Adresa { get; set; }

        [Required]
        [Display(Name = "Rating")]
        public double Ocena { get; set; }

        public List<Soba> ListSoba { get; set; }
    }
}