﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class Rezervacija
    {

        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string ImePrezime { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Od { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Do { get; set; }
        public Boolean Otkazana { get; set; } = false;


        public int SobaId { get; set; }
        public Soba Soba { get; set; }
        


    }
}