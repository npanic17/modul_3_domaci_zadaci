﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class RezervacijaContext: DbContext
    {
        public DbSet<Smestaj> Smestaj { get; set; }
        public DbSet<Soba> Soba { get; set; }
        public DbSet<Rezervacija> Rezervacija { get; set; }

        public RezervacijaContext():base("name=RezervacijasmestajaDBContext")
        {

        }
    }
}