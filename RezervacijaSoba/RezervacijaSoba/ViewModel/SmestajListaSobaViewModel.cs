﻿using RezervacijaSoba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.ViewModel
{
    public class SmestajListaSobaViewModel
    {
        public Smestaj Smestaj { get; set; }
        public List<Soba> ListaSoba { get; set; }
    }
}