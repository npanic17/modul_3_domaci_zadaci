namespace RezervacijaSoba.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rezervacijas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImePrezime = c.String(nullable: false, maxLength: 20),
                        Od = c.DateTime(nullable: false),
                        Do = c.DateTime(nullable: false),
                        Otkazana = c.Boolean(nullable: false),
                        SobaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sobas", t => t.SobaId, cascadeDelete: true)
                .Index(t => t.SobaId);
            
            CreateTable(
                "dbo.Sobas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BrojSobe = c.Int(nullable: false),
                        BrojKreveta = c.Int(nullable: false),
                        CenaPoNocenju = c.Double(nullable: false),
                        SmestajId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Smestajs", t => t.SmestajId, cascadeDelete: true)
                .Index(t => t.SmestajId);
            
            CreateTable(
                "dbo.Smestajs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(nullable: false, maxLength: 20),
                        Opis = c.String(nullable: false, maxLength: 20),
                        Adresa = c.String(nullable: false, maxLength: 20),
                        Ocena = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sobas", "SmestajId", "dbo.Smestajs");
            DropForeignKey("dbo.Rezervacijas", "SobaId", "dbo.Sobas");
            DropIndex("dbo.Sobas", new[] { "SmestajId" });
            DropIndex("dbo.Rezervacijas", new[] { "SobaId" });
            DropTable("dbo.Smestajs");
            DropTable("dbo.Sobas");
            DropTable("dbo.Rezervacijas");
        }
    }
}
