﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RezervacijaSoba.Models;

namespace RezervacijaSoba.Controllers
{
    public class SobasController : Controller
    {
        private RezervacijaContext db = new RezervacijaContext();

        


        // GET: Sobas
        public ActionResult Index()
        {
            var soba = db.Soba.Include(s => s.Smestaj);
            return View(soba.ToList());
        }

        // GET: Sobas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var soba = db.Soba.Find(id);
            var smestaj = db.Smestaj.Find(soba.SmestajId);
            soba.Smestaj = smestaj;
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // GET: Sobas/Create
        public ActionResult Create()
        {
            ViewBag.SmestajId = new SelectList(db.Smestaj, "Id", "Naziv");
            return View();
        }

        // POST: Sobas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BrojSobe,BrojKreveta,CenaPoNocenju,SmestajId")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Soba.Add(soba);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SmestajId = new SelectList(db.Smestaj, "Id", "Naziv", soba.SmestajId);
            return View(soba);
        }

        // GET: Sobas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            ViewBag.SmestajId = new SelectList(db.Smestaj, "Id", "Naziv", soba.SmestajId);
            return View(soba);
        }

        // POST: Sobas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BrojSobe,BrojKreveta,CenaPoNocenju,SmestajId")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Entry(soba).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SmestajId = new SelectList(db.Smestaj, "Id", "Naziv", soba.SmestajId);
            return View(soba);
        }

        // GET: Sobas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            var smestaj = db.Smestaj.Find(soba.SmestajId);
            soba.Smestaj = smestaj;
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // POST: Sobas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Soba soba = db.Soba.Find(id);
            db.Soba.Remove(soba);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
