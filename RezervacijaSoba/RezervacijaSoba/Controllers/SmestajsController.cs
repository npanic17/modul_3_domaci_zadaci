﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using RezervacijaSoba.Models;
using RezervacijaSoba.ViewModel;

namespace RezervacijaSoba.Controllers
{
    public class SmestajsController : Controller
    {
        private RezervacijaContext db = new RezervacijaContext();

        public enum SortType
        {
            Name,
            NameDesc,
            Rating,
            RatingDesc,
        }

        public static Dictionary<string, SortType> SortTypeDict = new Dictionary<string, SortType>
        {
            {"Name", SortType.Name },
            {"NameDesc", SortType.NameDesc },
            {"Rating", SortType.Rating },
            {"RatingDesc", SortType.RatingDesc },
        };


        // GET: Smestajs
        public ActionResult Index(string sortType = "Name")
        {
            SortType sortBy = SortTypeDict[sortType];

            IQueryable<Smestaj> list = db.Smestaj;

            switch (sortBy)
            {
                case SortType.Name:
                    list = list.OrderBy(p => p.Naziv);
                    break;
                case SortType.NameDesc:
                    list = list.OrderByDescending(p => p.Naziv);
                    break;
                case SortType.Rating:
                    list = list.OrderBy(p => p.Ocena);
                    break;
                case SortType.RatingDesc:
                    list = list.OrderByDescending(p => p.Ocena);
                    break;
            }
            ViewBag.sortTypes = new SelectList(SortTypeDict, "key", "key", sortType);
            ViewBag.currentsortype = sortType;
            return View(list.ToList());
        }

        // GET: Smestajs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Smestaj smestaj = db.Smestaj.Find(id);
            if (smestaj == null)
            {
                return HttpNotFound();
            }
            SmestajListaSobaViewModel vm = new SmestajListaSobaViewModel();
            vm.Smestaj = smestaj;
            vm.ListaSoba = db.Soba.Where(p => p.SmestajId == id).ToList();
            return View(vm);
        }

        // GET: Smestajs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Smestajs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv,Opis,Adresa,Ocena")] Smestaj smestaj)
        {
            if (ModelState.IsValid)
            {
                db.Smestaj.Add(smestaj);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(smestaj);
        }

        // GET: Smestajs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Smestaj smestaj = db.Smestaj.Find(id);
            if (smestaj == null)
            {
                return HttpNotFound();
            }
            return View(smestaj);
        }

        // POST: Smestajs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv,Opis,Adresa,Ocena")] Smestaj smestaj)
        {
            if (ModelState.IsValid)
            {
                db.Entry(smestaj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(smestaj);
        }

        // GET: Smestajs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Smestaj smestaj = db.Smestaj.Find(id);
            if (smestaj == null)
            {
                return HttpNotFound();
            }
            return View(smestaj);
        }

        // POST: Smestajs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Smestaj smestaj = db.Smestaj.Find(id);
            db.Smestaj.Remove(smestaj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Filter(SmestajFilter filter)
        {
            IQueryable<Smestaj> smestaj = db.Smestaj;

            if (!filter.Adresa.IsNullOrWhiteSpace())
            {
                smestaj = smestaj.Where(p => p.Adresa.Contains(filter.Adresa));
            }

            if (!filter.Opis.IsNullOrWhiteSpace())
            {
                smestaj = smestaj.Where(p => p.Opis.Contains(filter.Opis));
            }

            return View(smestaj.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
